const express = require('express');
const router = express.Router();
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const chatService = require('../services/chat.service');

router.get('/', authorize(Role.User), getAll);

module.exports = router;

function getAll(req, res, next) {
    chatService.getAll()
        .then(chats => res.json(chats))
        .catch(next);
}

