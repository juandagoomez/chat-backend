require('rootpath')();
var morgan = require('morgan')
const express = require('express');
const app = express();
var chatService = require('./services/chat.service');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const http = require("http").Server(app);
const io = require("socket.io");
const socket = io(http);
const cors = require('cors');
const errorHandler = require('_middleware/error-handler');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(morgan('dev'));

// allow cors requests from any origin and with credentials
app.use(cors({ origin: (_origin, callback) => callback(null, true), credentials: true }));

// api routes

app.use('/users', require('./controller/user.controller'));
app.use('/chats', require('./controller/chat.controller'));


// swagger docs route
app.use('/api-docs', require('_helpers/swagger'));

// global error handler
app.use(errorHandler);

socket.on("connection", (socket) => {
    console.log("usuario conectado");

    socket.on("disconnect", () => {
        console.log("usuario desconectado");
    });

    socket.on('new-message', (message) => {
          chatService.save(message);        
          socket.broadcast.emit('new-message', message);
        });
});

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
http.listen(port, () => {
    console.log('Server listening on port ' + port);
});




