const config = require('config.json');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const db = require('_helpers/db');
const Role = require('_helpers/role');
const User = db.User;

module.exports = {
    authenticate,
    refreshToken,
    registerAdmin,
    registerUser,
    addProject,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ nickName, password, ipAddress }) {
    const user = await User.findOne({ nickName });

    if (!user || !bcrypt.compareSync(password, user.passwordHash)) {
        throw 'user or password is incorrect';
    }

    // authentication successful so generate jwt and refresh tokens
    const jwtToken = generateJwtToken(user);
    const refreshToken = generateRefreshToken(ipAddress);

    // save refresh token
    user.refreshTokens.push(refreshToken);
    user.save();

    // return basic details and tokens
    return {
        ...basicDetails(user),
        jwtToken,
        refreshToken: refreshToken.token
    };
}

async function refreshToken({ token, ipAddress }) {
    const { user, refreshToken } = await getRefreshToken(token);

    // replace old refresh token with a new one and save
    const newRefreshToken = generateRefreshToken(ipAddress);
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    refreshToken.replacedByToken = newRefreshToken.token;
    user.refreshTokens.push(newRefreshToken);
    user.save();

    // generate new jwt
    const jwtToken = generateJwtToken(user);

    // return basic details and tokens
    return {
        ...basicDetails(user),
        jwtToken,
        refreshToken: newRefreshToken.token
    };
}



async function registerUser(params, origin) {

    // create user object
    const user = new User(params);
    user.role = Role.User;
    user.verificationToken = randomTokenString();

    // hash password
    if (params.password) {
        user.passwordHash = hash(params.password);
    }

    // save user
    await user.save();


}

async function registerAdmin(params, origin) {


    // create user object
    const user = new User(params);


    user.role = Role.Admin;
    user.verificationToken = randomTokenString();

    // hash password
    if (params.password) {
        user.passwordHash = hash(params.password);
    }

    // save user
    await user.save();


}

async function getAll() {
    const users = await User.find({ role: Role.User });
    return users.map(x => basicDetails(x));
}

async function getById(id) {
    const user = await getAccount(id);
    return basicDetails(user);
}

async function addProject(idUser, idProject) {
    const user = await getAccount(idUser);
    const users = await User.find({ projects: mongoose.Types.ObjectId(idProject) });
    if (users.length > 0) {
        throw 'Project "' + idProject + '" is already registered';
    }
    user.projects.push(idProject);

    await user.save();
    return basicDetails(user);
}



async function create(params) {
    // validate
    if (await User.findOne({ nickName: params.nickName })) {
        throw 'Usuario "' + params.nickName + '" is already registered';
    }

    const user = new User(params);
    user.verified = Date.now();

    // hash password
    if (params.password) {
        user.passwordHash = hash(params.password);
    }

    // save user
    await user.save();

    return basicDetails(user);
}

async function update(id, params) {
    const user = await getAccount(id);

    // validate
    if (user.nickName !== params.nickName && await User.findOne({ nickName: params.nickName })) {
        throw 'usuario "' + params.nickName + '" is already taken';
    }

    // copy params to user and save
    Object.assign(user, params);
    user.updated = Date.now();
    await user.save();

    return basicDetails(user);
}

async function _delete(id) {
    const user = await getAccount(id);
    await user.remove();
}

// helper functions

async function getAccount(id) {
    if (!db.isValidId(id)) throw 'User not found';
    const user = await User.findById(id);
    if (!user) throw 'User not found';
    return user;
}

async function getRefreshToken(token) {
    const user = await User.findOne().elemMatch('refreshTokens', { token });
    if (!user) throw 'Invalid token';
    const refreshToken = user.refreshTokens.find(x => x.token === token);
    if (!refreshToken.isActive) throw 'Invalid token';
    return { user, refreshToken };
}

function hash(password) {
    return bcrypt.hashSync(password, 10);
}

function generateJwtToken(user) {
    // create a jwt token containing the user id that expires in 15 minutes
    return jwt.sign({ sub: user.id, id: user.id }, config.secret, { expiresIn: '7d' });
}

function generateRefreshToken(ipAddress) {
    // create a refresh token that expires in 7 days
    return {
        token: randomTokenString(),
        expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
        createdByIp: ipAddress
    };
}

function randomTokenString() {
    return crypto.randomBytes(40).toString('hex');
}

function basicDetails(user) {
    const { id, name, nickName, typeUser, role } = user;
    return { id, name,nickName, typeUser, role };
}
