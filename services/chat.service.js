const db = require('_helpers/db');
const Chat = db.Chat;


module.exports = {
    getAll,
    save
};



async function getAll() {
    const chats = await Chat.find().populate('sender');
    return chats;
}

async function save(params) {
    var aux = { "message": params.message, "sender": params.sender.id }
    const chat = new Chat(aux);
    await chat.save();
}
