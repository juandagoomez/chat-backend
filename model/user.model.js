const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const refreshTokenSchema = new Schema({
    token: String,
    expires: Date,
    created: { type: Date, default: Date.now },
    createdByIp: String,
    revoked: Date,
    revokedByIp: String,
    replacedByToken: String
});

refreshTokenSchema.virtual('isExpired').get(function () {
    return Date.now() >= this.expires;
});

refreshTokenSchema.virtual('isActive').get(function () {
    return !this.revoked && !this.isExpired;
});

const User = new Schema({
    nickName: { type: String, unique: true, required: true },
    passwordHash: { type: String, required: true },
    role: { type: String, required: true },
    typeUser: { type: String, required: true },
    name: { type: String, required: true },
    verificationToken: String,
    resetToken: {
        token: String,
        expires: Date
    },
    created: { type: Date, default: Date.now },
    refreshTokens: [refreshTokenSchema]
});



User.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
        delete ret.passwordHash;
    }
});

module.exports = mongoose.model('User', User);